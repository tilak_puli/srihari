import {createReducer} from "@reduxjs/toolkit";
import {clearComments, clearPost, clearPosts, updateComments, updatePost, updatePosts, updateUsers} from "./action";

const initialState = {
    users:[],
    posts:[],
    post:{},
    comments: null
}

export default createReducer(initialState,{
    [updateUsers] : (state,action) => {
        state.users = action.payload.users;
    },
    [updatePosts] : (state,action) => {
        state.posts = action.payload.posts;
    },
    [clearPosts] : (state) => {
        state.posts = [];
    },
    [clearPost] : (state) => {
        state.post = {};
    },
    [clearComments] : (state) => {
        state.comments = null;
    },
    [updatePost] : (state,action) => {
        state.post = action.payload.post;
    },
    [updateComments] : (state,action) => {
        state.comments = action.payload.comments;
    },
})
