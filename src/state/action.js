import {createAction} from "@reduxjs/toolkit";

export const updateUsers = createAction("users/update",
    (users) => ({payload:{users}})
)

export const updatePosts = createAction("posts/update",
    (posts) => ({payload:{posts}})
)

export const clearPosts = createAction("posts/delete")

export const updatePost = createAction("post/update",
    (post) => ({payload:{post}})
)

export const clearPost = createAction("post/delete")

export const updateComments = createAction("comments/update",
    (comments) => ({payload:{comments}})
)

export const clearComments = createAction("comments/clear")

