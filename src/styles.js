import styled from "styled-components";

export const HeaderDiv = styled.div`
  width: 100%;
  height: 3em;
  min-height: 50px;
  
  display: flex;
  padding: 0.5em 1em;
`

export const Body = styled.div`
  padding:1em 3em;
  font-family: Muli,sans-serif!important;

`

export const Page = styled.div`
  padding:0 1em;
`

export const Heading = styled.div`
 font-weight: 600;
 color: #313131;
 font-size: 22px;
 line-height: 1.4;
`


export const Heading2 = styled.div`
 font-weight: 600;
 color: #313131;
 font-size: 18px;
 line-height: 1.4;
`


export const PostBody = styled.div`
 font-size: 16px;
 color: #646464;
`

export const RentomojoLogoContainer = styled.img`
  width: 180px;
  height: 40px;
`

export const UserTableDiv = styled.div`
    padding-top:2em;
    
    table {
        border: 1px solid #e2eaf0;
        border-radius: 0.4em;
    }
    
    thead {
        background:#f5f7fa;
    }
    
    th,td {
        line-height: 2;
        min-width: 20em;
        text-align: center;
    }
`


export const PostDiv = styled.div`
   max-width: 80%;
   margin-bottom:1em;
   
   border: 1px solid #e2eaf0;
   border-radius: 0.4em;
   
   padding: 1em;
   line-height: 2;
`

export const PostTitleDiv = styled.div`
  font-weight: 600;
  color: #313131;
  font-size: 18px;
`


export const HR = styled.hr`
  border-top: 1px solid #eee0;
  margin: 8px 0;
`

export const TitleDiv = styled.div`
  display:flex;
  width:100%;
  justify-content:space-between;
`

export const SmallMarginDiv = styled.div`
  margin-bottom: 2em;
`

export const CommentDiv = styled.div`
  padding: 0.5em 0;
  line-height: 1.5;
`

export const CommentTitle = styled.div`
  font-size: 16px;
  color: #787878;
`

export const SecondaryButton = styled.button`
  width: ${(props) => props.width || "10em"} ;
  padding: 10px;
  color: #1dbdc0;
  border: 1px solid #1dbdc0;
  border-radius: 30px;
}
`


export const BackIcon = styled.span`
  font-size: 20px;
  text-align:center;
  margin-right: 0.5em;
`

