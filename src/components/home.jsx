import React from "react";
import UserTable from "./user-table/user-table-container";
import { Heading} from "../styles";

const Home = () => (
    <div>
        <Heading>RentoMojo blog writers</Heading>
        <UserTable />
    </div>)

export default Home;
