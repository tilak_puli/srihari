import React  from "react";
import {getUsers} from "../../api"
import UserRow from "./user-row";
import {UserTableDiv} from "../../styles";

class UserTable extends React.Component{
    async componentDidMount (){
        const users = await getUsers();
        this.props.updateUsers(users);
    }

    render() {
        const users = this.props.users || [];
        return (
            <UserTableDiv>
             <table>
                    <thead>
                        <th>Name</th>
                        <th>Company</th>
                       <th>Blog posts</th>
                    </thead>
                    <tbody>
                      {users.map(user => <UserRow user={user}/>)}
                    </tbody>
            </table>
        </UserTableDiv>)
    }
}

export default UserTable;
