import Post from "./post";
import {connect} from "react-redux";
import {updatePost, updateComments, clearComments, clearPost} from "../../state/action";

const mapStateToProps = ({post,comments}) => ({post,comments})

const mapDispatchToProps = (dispatch) => ({
    updatePost:(posts) => dispatch(updatePost(posts)),
    updateComments:(comments) => dispatch(updateComments(comments)),
    clearPost:() => dispatch(clearPost()),
    clearComments:() => dispatch(clearComments()),
});


export default connect(mapStateToProps,mapDispatchToProps)(Post)
