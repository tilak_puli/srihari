import React  from "react";
import {getPost, getComments,deleteComment} from "../../api";
import {
    CommentDiv,
    SmallMarginDiv, CommentTitle,
    Heading,
    Heading2,
    HR,
    PostBody,
    SecondaryButton,
    TitleDiv
} from "../../styles";

class Post extends React.Component{
    async componentDidMount (){
        const posts = await getPost(this.props.match.params.id);
        this.props.updatePost(posts);
    }


    componentWillUnmount(){
        this.props.clearPost();
        this.props.clearComments();
    }

    async getComments(id){
        const comments = await getComments(id);
        this.props.updateComments(comments);
    }

    async deleteComment(id,userId){
        await deleteComment(id);
        this.goBackToPosts(userId);
    }

    goBackToPosts(userId){
        this.props.history.push(`/users/${userId}/posts`);
    }

    render() {
        const post = this.props.post || {};
        const comments = this.props.comments;

        return (
         <div>
            <SmallMarginDiv>
                <SmallMarginDiv>
                <SecondaryButton onClick={() => this.goBackToPosts(post.userId)} width={"8em"}
                >Back to Posts</SecondaryButton>
                </SmallMarginDiv>
            <TitleDiv>
              <Heading>{post.title}</Heading>
                <SecondaryButton onClick={() => this.deleteComment(post.id ,post.userId)}
                >Delete this post</SecondaryButton>
            </TitleDiv>
            <HR/>
            <PostBody>{post.body}</PostBody>
            </SmallMarginDiv>
            <div>
               <Comments comments={comments} getComments={this.getComments.bind(this,post.id)}/>
            </div>
        </div>)
    }
}

const Comments = ({comments,getComments}) => {
    if(!comments){
        return (<span>
                    <SecondaryButton onClick={getComments}>
                    Show Comments</SecondaryButton>
                </span>);
    }

    if(comments.length === 0){
        return <span> No comments. </span>
    }


    return (<smallPaddingDiv>
        <Heading2>Comments</Heading2>
        <div>{comments.map((comment,i) =>
            <Comment comment={comment} key={i}/>)}</div>
    </smallPaddingDiv>
)
}
const Comment = ({comment = {}}) => (
    <CommentDiv>
        <CommentTitle>{comment.name}</CommentTitle>
        <div>{comment.body}</div>
    </CommentDiv>
)

export default Post;
