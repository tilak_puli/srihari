import React  from "react";
import {getPosts} from "../../api";
import {Link} from "react-router-dom";
import {HR, PostDiv, PostTitleDiv, SecondaryButton} from "../../styles";

class Posts extends React.Component{
    async componentDidMount (){
        const posts = await getPosts(this.props.match.params.id);
        this.props.updatePosts(posts);
    }

    componentWillUnmount(){
        this.props.clearPosts();
    }

    render() {
        return (<div>
            {this.props.posts.map((post,i) => (<Post post={post} key={i}/>))}
        </div>)
    }
}

const Post = ({post}) =>
    (<PostDiv>
        <PostTitleDiv>{post.title}</PostTitleDiv>
        <HR />
        <span>{post.body}</span>
        <div>
            <SecondaryButton>
                <Link to={`/posts/${post.id}`}>View full post</Link>
            </SecondaryButton>
        </div>
    </PostDiv>)

export default Posts;
