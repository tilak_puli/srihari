import Posts from "./posts";
import {connect} from "react-redux";
import {clearPosts, updatePosts} from "../../state/action";

const mapStateToProps = ({posts}) => ({posts})

const mapDispatchToProps = (dispatch) => ({
    updatePosts:(posts) => dispatch(updatePosts(posts)),
    clearPosts:() => dispatch(clearPosts())
});


export default connect(mapStateToProps,mapDispatchToProps)(Posts)
