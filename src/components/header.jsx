import React from "react";
import {HeaderDiv, RentomojoLogoContainer} from "../styles";
import RentomojoLogo from "../resources/rentomojo-logo.png"
import {Link} from "react-router-dom";

const Header = () => (
    <HeaderDiv>
        <Link to={"/home"}><RentomojoLogoContainer src={RentomojoLogo}/></Link>
    </HeaderDiv>
)

export default Header;
